#include <fstream>
#include <vector>

#include "lzopd.hpp"

bool read_file(const std::string & filename, std::vector<uint8_t> & buffer)
{
    // Open file (seeking to end)
    std::ifstream ifs { filename, std::ios::in | std::ios::binary | std::ios::ate };
    if (!ifs)
    {
        ERROR_LOG << "Failed to open file: " << filename << '\n';
        return false;
    }

    // Allocate buffer
    buffer.resize(ifs.tellg());

    // Seek to beginning
    if (!ifs.seekg(0))
    {
        ERROR_LOG << "Failed to seek to beginning of file!\n";
        return false;
    }

    // Read into buffer
    if (!ifs.read(reinterpret_cast<char*>(buffer.data()), buffer.size()))
    {
        ERROR_LOG << "Failed to read file (" << filename << ")\n";
        return false;
    }

    return true;
}

int main()
{
    // Read file into buffer
    const std::string filename { "txt" };
    std::vector<uint8_t> input {};
    if (!read_file(filename, input))
    {
        ERROR_LOG << "Couldn't read input from file " << filename << '\n';
        return 1;
    }

    // Create lzopd class instance
    lzopd decompresser { input };

    // Decompress input into output
    std::vector<uint8_t> output;
    if (!decompresser.execute(output))
    {
        ERROR_LOG << "Failed to decompress input\n";
        return 1;
    }

    std::cout.write(reinterpret_cast<char *>(output.data()), output.size());

    return 0;
}

