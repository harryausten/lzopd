#ifndef _LZOPD_HPP_
#define _LZOPD_HPP_

#include <iostream>

#include "adler.hpp"
#include "crc.hpp"

// Error logging macro for better debug
#define ERROR_LOG std::cerr << "ERROR " << __FILE__ << " (" << __func__ << ':' << __LINE__ << ")\n\t"

// LZO header structure
struct header_t {
	uint16_t version_be16;
	uint16_t lib_version_be16;
	uint16_t version_needed_to_extract_be16;
	uint8_t  method;
	uint8_t	 level;
	uint32_t flags32;
	uint32_t mode_be32;
	uint32_t mtime_be32;
	uint32_t gmtdiff_be32;
	char     len_and_name[1+255+1];
};

// LZO header flags
enum class header_flags : uint32_t {
    adler32_d = 0x1U,
    adler32_c = 0x2U,
    extra_field = 0x40U,
    crc32_d = 0x100U,
    crc32_c = 0x200U,
    filter = 0x800U,
    crc32 = 0x1000U,
    mask = 0x3FFFU,
    // Operating system & file system that created the file
    os_mask = 0xFF000000U,
    // Character set for file name encoding
    cs_mask = 0x00F00000U,
    // These bits must be zero
    reserved = (mask | os_mask | cs_mask) ^ 0xFFFFFFFFU,
};

// LZO method types
enum class method_types : uint8_t {
	lzo1x_1    = 1,
	lzo1x_1_15 = 2,
	lzo1x_999  = 3,
};

// LZO file signature (magic bytes)
// The first nine bytes of an lzo file always contain the following values:
//
//                             0   1   2   3   4   5   6   7   8
//                           --- --- --- --- --- --- --- --- ---
// (hex)                      89  4c  5a  4f  00  0d  0a  1a  0a
// (decimal)                 137  76  90  79   0  13  10  26  10
// (C notation - ASCII)     \211   L   Z   O  \0  \r  \n \032 \n
static const uint8_t lzop_magic[9] =
    { 0x89, 0x4c, 0x5a, 0x4f, 0x00, 0x0d, 0x0a, 0x1a, 0x0a };

static constexpr uint32_t s_block_size { 256U * 1024U };

// Maximum LZO block size - DO NOT CHANGE!
static constexpr size_t s_max_block_size { 64U * 1024U * 1024U };

static inline uint32_t max_compressed_size(uint32_t sz)
{
    return sz + sz/16 + 64 + 3;
}

// Struct to hold checksum values for ADLER32 and CRC32
struct chksum_t {
	uint32_t f_adler32 { s_adler32_init_value };
	uint32_t f_crc32   { s_crc32_init_value   };
};

bool verify_chksum(chksum_t * chksum, uint32_t h_flags32, uint8_t * buf, uint32_t len, bool compress);

class lzopd
{
public:
    /// Constructor
    /// @param[in] input The input compressed bytes
    explicit lzopd(const std::vector<uint8_t> & input);

    /// @brief Perform the actual decompression
    /// @param[out] output The decompressed bytes
    /// @return True on success, false on failure
    bool execute(std::vector<uint8_t> & output);

private:
    /// Checksum count
    chksum_t m_chksum {};

    /// Current buffer position index
    size_t m_current_position { 0 };

    /// Input byte buffer vector
    const std::vector<uint8_t> & m_input;

    /// Output byte buffer vector
    std::vector<uint8_t> m_output {};

    /// Header flags
    uint32_t m_flags;

    void add_bytes_to_chksum(const void * buf, int cnt);
    uint32_t chksum_getresult();
    bool xread(void * buf, size_t len);
    bool f_read(void * buf, int cnt);
    bool f_read32(uint32_t * val);
    bool read32(uint32_t * val);
    bool check_magic(void);
    bool read_chksum(chksum_t * chksum);
    bool lzo_decompress();
    bool read_header();
    bool xwrite(const uint8_t * buf, size_t len);
};

#endif // _LZOPD_HPP_
