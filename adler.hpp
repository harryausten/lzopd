#ifndef _ADLER_HPP_
#define _ADLER_HPP_

// For uint8_t and uint32_t
#include <cstdint>

// For size_t
#include <cstddef>

/// Adler32 checksum initialisation value
static const uint32_t s_adler32_init_value { 1 };

/// @brief 16 sums unrolled
/// @param[in] buf The input buffer
/// @param[out] s1 First num
/// @param[out] s2 Second num
static inline void adler_do16(const uint8_t * buf, uint32_t & s1, uint32_t & s2)
{
    for (size_t i { 0 }; i < 16; ++i)
    {
        s1 += buf[i];
        s2 += s1;
    }
}

/// @brief Calculate the ADLER32 checksum
/// @param[in] adler Initial value of checksum
/// @param[in] buf The byte buffer
/// @param[in] len Length of the buffer
/// @return The checksum value
uint32_t adler32(uint32_t adler, const uint8_t * buf, size_t len);

/// @brief Verify a checksum for a given buffer
/// @param[in] buf The byte buffer
/// @param[in] len The length of the buffer
/// @param[in] ref The expected checksum value
/// @return True if correct, false if incorrect
bool adler_check(uint8_t * buf, unsigned len, uint32_t ref);

#endif // _ADLER_HPP_
