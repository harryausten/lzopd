## Introduction

This is an attempt to create a simple `C++` program/library for decrypting LZO encrypted data buffers.

This is intended to be used for extracting the Linux version string from a zImage (an LZO encrypted kernel binary).

## Steps

- [x] Take all code from [minilzo](https://www.oberhumer.com/opensource/lzo/) and [busybox](https://www.busybox.net/) (`lzop.c`) required for LZO decompression
- [x] Get it working as a standalone `C` program
- [x] Merge functions, condense and simplify code
- [x] Convert to `C++`
