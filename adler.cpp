#include "adler.hpp"

/// ADLER32 rolling hash base value
static const uint32_t s_adler_base { 65521 };

/// ADLER32 rolling hash nmax value
static const size_t   s_adler_nmax { 5552 };

uint32_t adler32(uint32_t adler, const uint8_t * buf, size_t len)
{
    // Return if buffer empty
    if (!buf)
    {
        return 1;
    }

    uint32_t s1 = adler & 0xffff;
    uint32_t s2 = (adler >> 16) & 0xffff;
    while (len > 0)
    {
        size_t k = len < s_adler_nmax ? len : s_adler_nmax;
        len -= k;
        if (k >= 16)
        {
            do
            {
                adler_do16(buf,s1,s2);
                buf += 16;
                k -= 16;
            }
            while (k >= 16);
        }
        if (k != 0)
        {
            do
            {
                s1 += *buf++;
                s2 += s1;
            }
            while (--k > 0);
        }
        s1 %= s_adler_base;
        s2 %= s_adler_base;
    }
    return (s2 << 16) | s1;
}

bool adler_check(uint8_t * buf, unsigned len, uint32_t ref)
{
    if (adler32(s_adler32_init_value, buf, len) != ref)
    {
        return false;
    }
    return true;
}
