#ifndef _CRC32_HPP_
#define _CRC32_HPP_

// For uint8_t and uint32_t
#include <cstdint>
// For size_t
#include <cstddef>

/// CRC32 checksum initialisation value
static const uint32_t s_crc32_init_value { 0 };

/// @brief Calculate the CRC32 checksum
/// @param[in] val The initialisation value
/// @param[in] buf The input byte buffer
/// @param[in] len Length of the buffer
/// @return The CRC32 checksum of the given buffer
uint32_t crc32(uint32_t val, const uint8_t * buf, size_t len);

/// @brief Verify the CRC32 checksum of a given buffer
/// @param[in] buf The byte buffer
/// @param[in] len Length of the buffer
/// @param[in] ref The expected checksum value
/// @return True if correct, false if incorrect
bool crc_check(uint8_t * buf, unsigned len, uint32_t ref);

#endif // _CRC32_HPP_
