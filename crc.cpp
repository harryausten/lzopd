#include "crc.hpp"

/// Ethernet/ZIP CRC32 polynomial in reversed bit order
static const uint32_t s_polynomial { 0xEDB88320 };

uint32_t crc32(uint32_t val, const uint8_t * buf, size_t len)
{
    val = ~val;
    while (len--)
    {
        val ^= *buf++;
        for (int k { 0 }; k < 8; ++k)
        {
            val = (val & 1) ? (val >> 1) ^ s_polynomial : val >> 1;
        }
    }
    return ~val;
}

bool crc_check(uint8_t * buf, unsigned len, uint32_t ref)
{
    uint32_t ret { crc32(s_crc32_init_value, buf, len) };
    if (ret != ref)
    {
        return false;
    }
    return true;
}
