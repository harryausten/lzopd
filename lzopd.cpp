// C system includes
extern "C"
{
#include <arpa/inet.h>
}

// C++ system includes
#include <vector>
#include <algorithm>

// LZO includes
#include <lzo/lzo1x.h>

// Local includes
#include "lzopd.hpp"

bool verify_chksum(chksum_t * chksum, uint32_t h_flags32, uint8_t * buf, uint32_t len, bool compress)
{
    const uint32_t adler_flag { (compress) ? static_cast<uint32_t>(header_flags::adler32_c) : static_cast<uint32_t>(header_flags::adler32_d) };
    const uint32_t crc_flag   { (compress) ? static_cast<uint32_t>(header_flags::crc32_c)   : static_cast<uint32_t>(header_flags::crc32_d)   };

    // Verify checksum of compressed block
    if (h_flags32 & adler_flag)
    {
        if (!adler_check(buf, len, chksum->f_adler32))
        {
            ERROR_LOG << "ADLER32 checksum error!\n";
            return false;
        }
    }
    if (h_flags32 & crc_flag)
    {
        if (!crc_check(buf, len, chksum->f_crc32))
        {
            ERROR_LOG << "CRC32 checksum error!\n";
            return false;
        }
    }
    return true;
}

lzopd::lzopd(const std::vector<uint8_t> & input)
    : m_input { input }
{}

bool lzopd::execute(std::vector<uint8_t> & output)
{
    // Check magic bytes
    if (!check_magic())
    {
        return false;
    }

    // Read header data
    if (!read_header())
    {
        return false;
    }

    // Run LZO decompression algorithm
    if (!lzo_decompress())
    {
        return false;
    }

    output = m_output;
    return true;
}

void lzopd::add_bytes_to_chksum(const void * buf, int cnt)
{
    // We need to handle the two checksums at once, because at the
    // beginning of the header, we don't know yet which one we'll
    // eventually need
    m_chksum.f_adler32 = adler32(m_chksum.f_adler32, (const uint8_t *)buf, cnt);
    m_chksum.f_crc32   =   crc32(m_chksum.f_crc32,   (const uint8_t *)buf, cnt);
}

uint32_t lzopd::chksum_getresult()
{
    return (m_flags & static_cast<uint32_t>(header_flags::crc32)) ? m_chksum.f_crc32 : m_chksum.f_adler32;
}

bool lzopd::xread(void * buf, size_t len)
{
    // Return successfully if no bytes to read
    if (!len)
    {
        return true;
    }

    // Check valid read length
    if (m_current_position + len > m_input.size())
    {
        ERROR_LOG << "Invalid read!\n";
        return false;
    }

    // Copy memory and increment current position pointer
    std::copy_n(&m_input[m_current_position], len, reinterpret_cast<uint8_t *>(buf));
    m_current_position += len;

    return true;
}

bool lzopd::f_read(void * buf, int cnt)
{
    if (!xread(buf, cnt))
    {
        return false;
    }
    add_bytes_to_chksum(buf, cnt);
    return true;
}

bool lzopd::f_read32(uint32_t * val)
{
    if (!f_read(val, 4))
    {
        ERROR_LOG << "Failed to read uint32_t!\n";
        return false;
    }
    *val = ntohl(*val);
    return true;
}

bool lzopd::read32(uint32_t * val)
{
    if (!xread(val, 4))
    {
        ERROR_LOG << "Failed to read uint32_t!\n";
        return false;
    }
    *val = ntohl(*val);
    return true;
}

bool lzopd::check_magic(void)
{
    uint8_t magic[sizeof(lzop_magic)];
    if (!xread(magic, sizeof(magic)))
    {
        return false;
    }

    if (!std::equal(std::begin(magic), std::end(magic), lzop_magic))
    {
        ERROR_LOG << "Bad magic bytes! Expected:\n";
        for (const auto & b : lzop_magic)
        {
            ERROR_LOG << "0x" << std::hex << static_cast<int>(b) << ' ';
        }
        ERROR_LOG << "Got:\n";
        for (const auto & b : magic)
        {
            ERROR_LOG << "0x" << std::hex << static_cast<int>(b) << ' ';
        }
        return false;
    }
    return true;
}

bool lzopd::read_chksum(chksum_t * chksum)
{
    // If ADLER32 header flag, read ADLER32 checksum
    if (m_flags & static_cast<uint32_t>(header_flags::adler32_d))
    {
        if (!read32(&chksum->f_adler32))
        {
            return false;
        }
    }
    // If CRC32 header flag, read CRC32 checksum
    if (m_flags & static_cast<uint32_t>(header_flags::crc32_d))
    {
        if (!read32(&chksum->f_crc32))
        {
            return false;
        }
    }
    return true;
}

bool lzopd::lzo_decompress()
{
    uint32_t block_size { s_block_size };
    uint32_t mcs_block_size { max_compressed_size(block_size) };
    uint8_t * b2 { nullptr };

    while (true)
    {
        // Read uncompressed block size
        uint32_t dst_len { 0 };
        if (!read32(&dst_len))
        {
            ERROR_LOG << "Failed to read uncompressed data block size!\n";
            return false;
        }

        // Exit if last block
        if (dst_len == 0)
        {
            break;
        }

        // Error if split file
        if (dst_len == 0xffffffffL)
        {
            // Should not happen - not yet implemented
            ERROR_LOG << "this file is a split lzop file\n";
            return false;
        }

        // Error if too long
        if (dst_len > s_max_block_size)
        {
            ERROR_LOG << "corrupted data: read block of uncompressed data larger than " << s_max_block_size << '\n';
            return false;
        }

        // Read compressed block size
        uint32_t src_len { 0 };
        if (!read32(&src_len))
        {
            ERROR_LOG << "Failed to read compressed data block size!\n";
            return false;
        }
        if (src_len == 0 || src_len > dst_len)
        {
            ERROR_LOG << "corrupted data: failed length reads. Got src_len = " << src_len << ", dst_len = " << dst_len << '\n';
            return false;
        }

        if (dst_len > block_size)
        {
            if (b2)
            {
                delete[] b2;
            }
            block_size = dst_len;
            mcs_block_size = max_compressed_size(block_size);
        }

        // Read checksum of uncompressed block
        chksum_t uncompressed_chksum {};
        if (!read_chksum(&uncompressed_chksum))
        {
            ERROR_LOG << "Failed to read uncompressed data block checksum!\n";
            return false;
        }

        // Read checksum of compressed block
        chksum_t compressed_chksum {};
        if (src_len < dst_len)
        {
            if (!read_chksum(&compressed_chksum))
            {
                ERROR_LOG << "Failed to read compressed data block checksum!\n";
                return false;
            }
        }

        // If b2 not set, allocate it
        if (!b2)
        {
            b2 = new uint8_t[mcs_block_size];
            std::fill(b2, b2 + mcs_block_size, 0);
        }

        // Read the block into the end of our buffer
        uint8_t * b1 { b2 + mcs_block_size - src_len };
        if (!xread(b1, src_len))
        {
            ERROR_LOG << "Failed to combine buffers!\n";
            return false;
        }

        // Decrypted data buffer
        uint8_t * dst { nullptr };
        if (src_len < dst_len)
        {
            // Verify checksum of compressed block
            if (!verify_chksum(&compressed_chksum, m_flags, b1, src_len, true))
            {
                ERROR_LOG << "Checksum verification failed!\n";
                return false;
            }

            // Decompress
            size_t d { dst_len };
            int ret { lzo1x_decompress_safe(b1, src_len, b2, &d, NULL) };
            if (ret)
            {
                ERROR_LOG << "LZO Decompression Failed! Returned " << ret << '\n';
                return false;
            }
            if (dst_len != d)
            {
                ERROR_LOG << "LZO Decompression Failed! Read " << d << " bytes, but expected " << dst_len << '\n';
                return false;
            }
            dst = b2;
        }
        else
        {
            // "stored" block => no decompression
            dst = b1;
        }

        // Verify checksum of uncompressed block
        if (!verify_chksum(&uncompressed_chksum, m_flags, dst, dst_len, false))
        {
            ERROR_LOG << "Checksum verification failed!\n";
            return false;
        }

        // Write uncompressed block data
        xwrite(dst, dst_len);
    }

    delete[] b2;
    return true;
}

bool lzopd::read_header()
{
    header_t h {};
    m_chksum = { s_adler32_init_value, s_crc32_init_value };

    // Read up to and including name length byte
    f_read(&h.version_be16, ((char *)&h.len_and_name[1]) - ((char *)&h.version_be16));

    const uint32_t h_version { htons(h.version_be16) };
    // Require a version greater than or equal to 0.94
    // (released 15/10/97), due to header format change
    if (h_version < 0x0940)
    {
        ERROR_LOG << "Header corrupted - version number too small\n";
        return false;
    }
    const uint32_t h_version_needed_to_extract { htons(h.version_needed_to_extract_be16) };
    // Current decryption algorithm taken from LZOP version 1.01
    // (released 27/04/03)
    if (h_version_needed_to_extract > 0x1010)
    {
        ERROR_LOG << "Need a newer version of LZOP decryption algorithm!\n";
        return false;
    }
    if (h_version_needed_to_extract < 0x0940)
    {
        ERROR_LOG << "Header corrupted - needed version number too small\n";
        return false;
    }

    if (h.method <= 0)
    {
        ERROR_LOG << "Unknown compression method. Update LZOP decryption algorithm?\n";
        return false;
    }

    // Get LZO method type
    if (h.method == static_cast<uint8_t>(method_types::lzo1x_1))
    {
        if (h.level == 0)
        {
            h.level = 3;
        }
    }
    else if (h.method == static_cast<uint8_t>(method_types::lzo1x_1_15))
    {
        if (h.level == 0)
        {
            h.level = 1;
        }
    }
    else if (h.method == static_cast<uint8_t>(method_types::lzo1x_999))
    {
        if (h.level == 0)
        {
            h.level = 9;
        }
    }
    else
    {
        ERROR_LOG << "Unkown compression method. Update LZOP decryption algorithm?\n";
        return false;
    }

    // Check compression level
    if (h.level < 1 || h.level > 9)
    {
        ERROR_LOG << "Unkown compression level. Update LZOP decryption algorithm?\n";
        return false;
    }

    // Fix flags endianness and assign member var
    h.flags32 = ntohl(h.flags32);
    m_flags = h.flags32;

    if (h.flags32 & static_cast<uint32_t>(header_flags::filter))
    {
        ERROR_LOG << "Filter not supported. Update LZOP decryption algorithm?\n";
        return false;
    }
    // Check reserved flags
    if (h.flags32 & static_cast<uint32_t>(header_flags::reserved))
    {
        ERROR_LOG << "Unknown (reserved) flags set. Update LZOP decryption algorithm?\n";
        return false;
    }

    const int l { h.len_and_name[0] };
    if (l > 0)
    {
        f_read(h.len_and_name + 1, l);
    }

    uint32_t checksum { chksum_getresult() };
    uint32_t real_sum { 0 };
    if (!read32(&real_sum) || real_sum != checksum)
    {
        ERROR_LOG << "Header corrupted - checksum error!\n";
        return false;
    }

    // Skip extra field (not used yet)
    if (h.flags32 & static_cast<uint32_t>(header_flags::extra_field))
    {
        // Note: the checksum also covers the length
        m_chksum = { s_adler32_init_value, s_crc32_init_value };
        uint32_t extra_field_len { 0 };
        if (!f_read32(&extra_field_len))
        {
            ERROR_LOG << "Failed to read extra field length!\n";
            return false;
        }

        char dummy;
        for (uint32_t k { 0 }; k < extra_field_len; ++k)
        {
            f_read(&dummy, 1);
        }

        checksum = chksum_getresult();
        uint32_t extra_field_checksum { 0 };
        if (!read32(&extra_field_checksum))
        {
            ERROR_LOG << "Failed to read extra field checksum!\n";
            return false;
        }
        if (extra_field_checksum != checksum)
        {
            ERROR_LOG << "Header corrupted - checksum error!\n";
            return false;
        }
    }

    return true;
}

bool lzopd::xwrite(const uint8_t * buf, size_t len)
{
    // Return successfully if no bytes to write
    if (!len)
    {
        return true;
    }

    // Copy bytes into output vector
    std::copy_n(buf, len, std::back_inserter(m_output));

    return true;
}
