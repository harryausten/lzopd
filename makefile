WARN = -Wall -Wextra -Wpedantic
OPT = -Og -g -mtune=native -march=native
DATA = Some text to be compressed

%.o: %.cpp
	$(CXX) -c $(WARN) $(OPT) $^ -o $@ -llzo2

lzopd: lzopd.o crc.o adler.o main.o
	$(CXX) $(WARN) $(OPT) $^ -o $@ -llzo2

txt:
	@echo "$(DATA)" | lzop > $@

.PHONY: test
test: lzopd txt
	@if ! ./lzopd > out; then \
		printf "\nProgram errored!\n"; \
	fi
	@if [ "$(DATA)" != "`cat out`" ]; then \
		printf "Expected:\n\t%s\nGot:\n\t%s\n" "'$(DATA)'" "'`cat out`'"; \
	else \
		echo "SUCCESS!"; \
	fi
	@rm out

.PHONY: clean
clean:
	$(RM) lzopd txt
